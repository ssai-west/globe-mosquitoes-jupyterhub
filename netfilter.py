from netfilter.rule import Rule, Match
from netfilter.table import Table

rule = Rule(in_interface='eth0', protocol='tcp', matches=[Match('tcp')], jump='ACCEPT')
table = Table('filter')
table.append_rule('INPUT', rule)

table.delete_rule('INPUT', rule)

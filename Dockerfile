# Do not forget to pin down the version
FROM jupyterhub/configurable-http-proxy
FROM jupyterhub/singleuser:0.9
FROM jupyterhub/jupyterhub:0.9

# Copy the JupyterHub configuration in the container
COPY jupyterhub_config.py .
COPY fullchain.pem .
COPY privkey.pem .

Type here to test something

#ENV DOCKER_NETWORK_NAME=${DOCKER_NETWORK_NAME}
#ENV DOCKER_NETWORK_NAME=jupyterhub-network


# # Download script to automatically stop idle single-user servers
# RUN wget https://raw.githubusercontent.com/jupyterhub/jupyterhub/0.9.3/examples/cull-idle/cull_idle_servers.py
#
# Install dependencies (for advanced authentication and spawning)
RUN pip install \
    notebook==6.0.3 \
    dockerspawner==0.10.0 \
    jupyterhub-simplespawner==0.1 \
    jupyterhub-nativeauthenticator


EXPOSE 8000

# attempt to block traffic
#RUN iptables -I DOCKER -i ext_if ! -s 8.8.8.8 -j DROP
#-N DOCKER
#iptables -N DOCKER-USER
#RUN pfctl